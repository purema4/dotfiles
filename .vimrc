syntax on
"setlocal spell spelllang=ca
set number
set relativenumber

" Quickly insert an empty new line without entering insert mode
nnoremap <Leader>o o<Esc>
nnoremap <Leader>O O<Esc>
set path+=**
set wildmenu
set nocompatible
filetype plugin on
command! MakeTags !ctags -R .
":inoremap ii <ESC>
